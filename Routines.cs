#define EASING

using UnityEngine;
using System.Collections;
using System;
#if EASING
using EasingFunctions;
#endif

public class Routines
{
	public static IEnumerator Wait (float time)
	{
		while(0 < time)
		{
			time -= Time.deltaTime;
			yield return null;
		}
	}
	
	public static IEnumerator WaitUntil (Func<bool> condition)
	{
		while(condition()) yield return null;
	}
	
	public static IEnumerator Action (Action action)
	{
		if(action != null) action();
		yield break;
	}
	
	public static IEnumerator DelayedAction (float time, Action action)
	{
		var wait = Wait(time);
		while(wait.MoveNext()) yield return null;
		if(action != null) action();
	}
	
	public static IEnumerator DelayedAction <A> (float time, A param1, Action<A> action)
	{
		var wait = Wait(time);
		while(wait.MoveNext()) yield return null;
		if(action != null) action(param1);
	}
	
	public static IEnumerator DelayedAction <A,B> (float time, A param1, B param2, Action<A,B> action)
	{
		var wait = Wait(time);
		while(wait.MoveNext()) yield return null;
		if(action != null) action(param1, param2);
	}
	
	public static IEnumerator DelayedAction <A,B,C> (float time, A param1, B param2, C param3, Action<A,B,C> action)
	{
		var wait = Wait(time);
		while(wait.MoveNext()) yield return null;
		if(action != null) action(param1, param2, param3);
	}
	
	public static IEnumerator Interpolate (float time, Action<float> action)
	{
		float t = 0;
		while(t < time)
		{
			action(t/time);
			yield return null;
			t += Time.deltaTime;
		}
		action(1);
	}
	
#if EASING
	public static IEnumerator Interpolate (float time, Action<float> action, EaseType easeType)
	{
		var ease = Easing.GetFunction(easeType);
		return Interpolate(time, action, ease);
	}
	
	public static IEnumerator Interpolate (float time, Action<float> action, EasingFn ease)
	{
		float t = 0;
		while(t < time)
		{
			action(ease(0,1,t/time));
			yield return null;
			t += Time.deltaTime;
		}
		action(1);
	}
#endif
}