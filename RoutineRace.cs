﻿using System.Collections.Generic;
using System.Collections;

public class RoutineRace : List<IEnumerator>
{
	public RoutineRace (params IEnumerator[] routines)
	{
		AddRange(routines);
	}
	
	public void Add (params IEnumerator[] routines)
	{
		AddRange(routines);
	}
	
	public bool MoveNext ()
	{
		for(int i = 0; i < Count; i++)
		{
			if(!this[i].MoveNext())
			{
				return false;
			}
		}
		return true;
	}
	
	public IEnumerator ToRoutine ()
	{
		while(MoveNext()) yield return null;
	}
	
	bool isDone = false;
	public bool IsDone { get{ return Count == 0; } }
}