using System.Collections.Generic;
using System.Collections;

public class RoutineQueue : List<IEnumerator>
{
	public RoutineQueue (params IEnumerator[] routines)
	{
		AddRange(routines);
	}
	
	public bool MoveNext ()
	{
		if(Count == 0) return false;
		if(!this[0].MoveNext())
		{
			// In case the current coroutine cleared this queue...
			if(Count == 0) return false;
			RemoveAt(0);
			return Count != 0;
		}
		return true;
	}
	
	public IEnumerator ToRoutine ()
	{
		while(MoveNext()) yield return null;
	}
	
	public bool IsDone { get{ return Count == 0; } }
}