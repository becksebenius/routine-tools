using System.Collections.Generic;
using System.Collections;

public class RoutineSet : List<IEnumerator>
{
	public RoutineSet (params IEnumerator[] routines)
	{
		AddRange(routines);
	}
	
	public void Add (params IEnumerator[] routines)
	{
		AddRange(routines);
	}
	
	public bool MoveNext ()
	{
		bool b = false;
		for(int i = 0; i < Count; i++)
		{
			if(!this[i].MoveNext())
			{
				//in case routine changed count
				if(i < Count)
					RemoveAt(i--);
			}
			else b = true;
		}
		return b;
	}
	
	public IEnumerator ToRoutine ()
	{
		while(MoveNext()) yield return null;
	}
	
	public bool IsDone { get{ return Count == 0; } }
}