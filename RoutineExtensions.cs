using System.Collections.Generic;
using System.Collections;
using System;

public static class RoutineExtensions
{
	public static bool IsDone (this IEnumerator routine) 
	{
		try
		{
			var obj = routine.Current;
			return false;
		}
		catch(Exception e)
		{
			return true;
		}
	}
	
	public static bool SeekValue (this IEnumerator routine)
	{
		if(!routine.MoveNext()) return false;
		if(routine.Current != null) return false;
		return true;
	}
	
	public static void Continue (this IEnumerator routine)
	{
		routine.MoveNext();
	}
}